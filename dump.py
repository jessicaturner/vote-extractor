# Required for rest of hug scripts
from dxpchain import DxpChain
from dxpchain.account import Account
from dxpchain.instance import set_shared_dxpchain_instance # Used to reduce dxpchain instance load
import dxpchain
import sys
import ujson # For outputting to disk
from progressbar import Bar, ETA, ProgressBar, Percentage # For providing progressbar functionality. This is actually "progressbar33" in python3.

"""
Configure Full/API node for querying network info
Only enable ONE of the following API nodes!
"""
full_node_url = 'ws://localhost:11011' 
dxpchain_full_node = DxpChain(full_node_url, nobroadcast=False)
set_shared_dxpchain_instance(dxpchain_full_node)
# End of node configuration

def write_json_to_disk(json_data, start, end):
	"""
	When called, write the json_data to a json file.
	We will end up with many vote_data_*_*.json files.
	These files will be merged using jq.
	"""
	filename = "vote_data_" + str(start) + "_" + str(end)
	with open(filename, 'w') as outfile:
		ujson.dump(json_data, outfile, encode_html_chars=False, escape_forward_slashes=False, ensure_ascii=False) #Write JSON to data.json (disk)

def dump_vote_tx(start, end):
	"""Enable retrieving and displaying any DXP object in JSON."""
	vote = {}
	vote['votes'] = []

	widgets = [Percentage(), # Setting how we wan the progress bar to look
			   ' ', Bar(),
			   ' ', ETA()]

	input_range = end - start
	scrape_range_ref = input_range + 1
	pbar = ProgressBar(widgets=widgets, maxval=scrape_range_ref).start() #Prepare the progress bar
	progress_iterator = 0

	for x in range(start, end):
		pbar.update(progress_iterator + 1) # Display incremented progress
		progress_iterator += 1 # Iterate the progress bar for next iteration

		object_id = "1.11." + str(x)
		try:
			retrieved_object = dxpchain_full_node.rpc.get_objects([object_id])[0]
		except:
			continue

		if retrieved_object is not None:
			if 'new_options' in retrieved_object.keys():
				# Log details!
				print("Vote!")
				vote['votes'].append(retrieved_object)
			#else:
				#print("Not a vote!")
	return vote

if __name__ == '__main__':
	scrape_start = int(sys.argv[1]) # Scrape tx from
	scrape_stop = int(sys.argv[2]) # Scrape tx to
	if (scrape_start < scrape_stop and scrape_start >= 0):
		dumped_vote_data = dump_vote_tx(scrape_start, scrape_stop) # Extract account update tx
		write_json_to_disk(dumped_vote_data, scrape_start, scrape_stop) # outputs into file 'vote_data.json'
	else:
		print("Input valid input data.")
